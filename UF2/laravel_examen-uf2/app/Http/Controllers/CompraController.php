<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\CompraRequest;
use App\Http\Requests\RegisterRequest;

class CompraController extends Controller
{

    public $product;
    public function __construct()
    {
        $this->product = new Product;
    }

    public function main()
    {
        /*Recuerda el estado de la compra y redirige a la pantalla en la que el usuario estaba antes: resumen, envio o confirmar */
        return redirect('/compra/resumen');
    }
    /**
     * Method to show the resume of the products in the chart
     */
    public function resumen(CompraRequest $request)
    {
        var_dump($request->input('prod_id'));
        $prod = $this->product->productValues($request->input('prod_id'));
        //Dummy: hay que cambiar la info por la información guardada en el carrito (session)
        $products = [
            (object) [
                'name' => $prod->value('name'), 'category' => $prod->value('category'), 'stock' => $prod->value('stock'), 'description' => $prod->value('description'), 'price' => $prod->value('price'), 'image' => $prod->value('image'), 'rating' => $prod->value('rating')
            ]
        ];
        return view('compra/resumen')
            ->with('products', $products);
    }

    public function addToCart(Request $request) {
        $array = array('id' => $request->input('id'),'name' => $request->input('name'),'description' => $request->input('description'),
        'category' => $request->input('category'), 'stock' => $request->input('stock'), 'rating' => $request->input('rating'), 'price' => $request->input('price'), 'image' => $request->input('image'));
        $carrito = $request->session()->get('carrito',[]);
        array_push($carrito,$array);
        $request->session()->put('carrito',$carrito);
        return redirect(url()->previous());
    }


    /**
     * Method to show and process the shipping form (envio)
     */
    public function envio()
    {
        return view('compra/envio');
    }
    /**
     * Method to show and process the shipping form (envio)
     */
    public function verificarEnvio(RegisterRequest $request)
    {
        $formOK = false;
        /* PONER AQUI TODO LO NECESARIO PARA VERIFICAR EL FORMULARIO */
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->direccion = $request->input('direccion');
        $user->password = $request->input('password');
        $user->passwordConfirm = $request->input('password-confirm');
        $user->foto = $request->foto->getClientOrigialName();
        /*Una vez verificado se guarda la información de envio en la session*/

        //si el formulario se ha rellenado correctamente se redirecciona a la pagina de confirmación
        /* if ($formOK) redirect('/compra/confirmar');
        return view('compra/envio'); */
        return view('/compra/confirmar', with(['user'=>$this->user->validate($user->name, $user->email, $user->direccion, $user->foto)]));
    }
    /**
     * Method to show the list of procuts and shipping info
     */
    public function confirmar()
    {
        //Dummy: hay que cambiar la info por la información guardada en session
        $products = [
            (object) [
                'name' => 'Lego 1', 'category' => 0, 'description' => 'Esto es la descripcion lego 1', 'price' => 10.02, 'image' => 'lego1.jpeg', 'rating' => 2
            ]
        ];
        //Dummy: hay que cambiar la info por la información guardada en session
        $shipping =   (object)[
            'name' => 'Pedro',
            'mail' => 'asds|@asda.es',
            'address' => 'asds|@asda.es',
            'image' => 'lego1.jpeg',
        ];
        return view('compra/confirmar')->with('products', $products)->with('shipping', $shipping);
    }
}
