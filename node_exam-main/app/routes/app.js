var express = require("express");
var router = express.Router();
var path = require("path");
var ctrlDir = "/app/app/controllers";
var alumneCtrl = require(path.join(ctrlDir, "alumnos"));
var docenteCtrl = require(path.join(ctrlDir, "docentes"));
var asignaturaCtrl = require(path.join(ctrlDir, "asignaturas"));

router.get("/asignatura/new", async function(req, res, next) {
    alumnos = await alumneCtrl.list(req);
    docentes = await docenteCtrl.list(req);        
    res.render("newAsignatura",{listaAlumnos: alumnos, listaDocentes: docentes})
});

router.post("/asignatura/new", async function(req, res, next) {
    result = await asignaturaCtrl.addAsignatura(req);
    res.send("Assignatura añadida correctamente");
});


router.get("/chat", function(req, res, next) {
    res.render("chat")
});

router.get("/chat/:id", function(req, res, next) {
    var sala = req.params.id;
    console.log(sala);
    res.render("chat")
});

module.exports = router;

