var express = require("express");
var router = express.Router();
var path = require("path");
//Controllers
var ctrlDir = "/app/app/controllers";
var alumneCtrl = require(path.join(ctrlDir, "alumnos"));
var docenteCtrl = require(path.join(ctrlDir, "docentes"));

// Alumnos
router.get("/alumnos", alumneCtrl.list);
router.get("/alumnos/:id", alumneCtrl.listAlumno);
router.post("/alumnos", alumneCtrl.addAlumno);
router.delete("/alumnos/:id", alumneCtrl.deleteAlumno);
router.put("/alumnos/:id", alumneCtrl.updateAlumno);

// Docentes
router.get("/docentes", docenteCtrl.list);
router.get("/docentes/:id", docenteCtrl.listDocente);
router.post("/docentes", docenteCtrl.addDocente);
router.delete("/docentes/:id", docenteCtrl.deleteDocente);
router.put("/docentes/:id", docenteCtrl.updateDocente);

module.exports = router;
