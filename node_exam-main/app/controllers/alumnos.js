var mongoose = require("mongoose"),
Alumno = require("../models/alumnos");

// list
exports.list = async (req, res, next) => {
    const alumnos = await Alumno.find();
    if (req.isApi) {
        res.json(alumnos);
    } else {
        return alumnos;
    }
}

// listAlumno
exports.listAlumno = async (req, res, next) => {
    const alumno = await Alumno.findById(req.params.id);
    if(req.isApi) {
        res.json(alumno);
    } else {
        return alumno;
    }    
}

// addAlumno
exports.addAlumno = async (req, res, next) => {
    const alumno = new Alumno({
        nombre: req.body.nombre,
        apellido: req.body.apellido
    })
    const add = await alumno.save();
    if(req.isApi) {
        res.status(201).json(add);
    } else {
        return add;
    }
}

// deleteAlumno
exports.deleteAlumno = async (req, res) => {
    const alumno = await Alumno.findById(req.params.id);
    const resultado = alumno.delete();
    if (req.isApi) {
        res.json({ mesagge: "Se ha borrado correctamente"});
    } else {
        return "Se ha borrado correctamente";
    } 
}

// updateAlumno
exports.updateAlumno = async (req,res) => {
    const alumno = {
        nombre: req.body.nombre,
        apellido: req.body.apellido
    }
    const actu = await Alumno.updateOne({_id: req.params.id}, alumno)
    if(req.isApi) {
        res.status(200).json({ mesagge: "Se ha actualizado correctamente"});
    } else {
        return "Se ha actualizado correctamente";
    }
}
