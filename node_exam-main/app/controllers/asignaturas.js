var mongoose = require("mongoose"),
Asignatura = require("../models/asignatura");

// listAsignaturas
exports.listAsignaturas = (req, res, next) => {
    var asignaturas = [
        {
            id: 1,
            nombre: "Laravel",
            numHoras: 50,
            docente: {
                rel: "docente",
                method: "GET",
                href: "http://localhost:3000/api/docentes/1"
            },
            alumnos: [
                {
                    rel: "alumno",
                    method: "GET",
                    href: "http://localhost:3000/api/alumnos/1"
                },
                {
                    rel: "alumno",
                    method: "GET",
                    href: "http://localhost:3000/api/alumnos/2"
                }
            ]
        },
        {
            id: 2,
            nombre: "Node",
            numHoras: 30,
            docente: {
                rel: "docente",
                method: "GET",
                href: "http://localhost:3000/api/docentes/2"
            },
            alumnos: [
                {
                    rel: "alumno",
                    method: "GET",
                    href: "http://localhost:3000/api/alumnos/1"
                },
                {
                    rel: "alumno",
                    method: "GET",
                    href: "http://localhost:3000/api/alumnos/2"
                }
            ]
        }
    ]
    return res.json(asignaturas);
}

// addAsignatura
//exports.addAsignatura = (req, res, next) => {
//
//}

// updateAsignatura
exports.updateAsignatura = (req, res, next) => {
    return res.status(200).send(  {
        id: 1,
        nombre: "PHP",
        numHoras: 40,
        docente: {
            rel: "docente",
            method: "GET",
            href: "http://localhost:3000/api/docentes/1"
        },
        alumnos: [
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/1"
            },
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/2"
            },
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/3"
            }
        ]
    }
   );
}

// deleteAsignatura
exports.deleteAsignatura = (req, res, next) => {
    return res.status(200).send(  {
        id: 1,
        nombre: "Laravel",
        numHoras: 50,
        docente: {
            rel: "docente",
            method: "GET",
            href: "http://localhost:3000/api/docentes/1"
        },
        alumnos: [
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/1"
            },
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/2"
            }
        ]
    }
   );
}
