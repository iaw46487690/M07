var mongoose = require("mongoose"),
Docente = require("../models/docente");

// list
exports.list = async (req, res, next) => {
    const docente = await Docente.find();
    if (req.isApi) {
        res.json(docente);
    } else {
        return docente;
    }
}

// listDocente
exports.listDocente = async (req, res, next) => {
    console.log(req.params.id);
    const docente = await Docente.findById(req.params.id);
    if(req.isApi) {
    res.json(docente);
    } else {
        return docente;
    } 
}

// addDocente
exports.addDocente = async (req, res, next) => {
    const docente = new Docente({
        nombre: req.body.nombre,
        apellido: req.body.apellido
    })
    const add = await docente.save();
    if(req.isApi) {
        res.status(201).json(add);
    } else {
        return add;
    }
}

// deleteDocente
exports.deleteDocente = async (req, res) => {
    const docente = await Docente.findById(req.params.id);
    const resultado = docente.delete();
    if (req.isApi) {
        res.json({ mesagge: "Se ha borrado correctamente"});
    } else {
        return "Se ha borrado correctamente";
    }
}

// updateDocente
exports.updateDocente = async (req,res) => {
    const docente = {
        nombre: req.body.nombre,
        apellido: req.body.apellido
    }
    const update = await Docente.updateOne({_id: req.params.id}, docente)
    if(req.isApi) {
        res.status(200).json({ mesagge: "Se ha actualizado correctamente"});
    } else {
        return "Se ha actualizado correctamente";
    }
}
