var mongoose = require('mongoose');
Schema = mongoose.Schema;

var DocenteSchema = new mongoose.Schema({
    // _id: {type: Object},
    nombre: {type: String},
    apellido: {type: String},  
});

module.exports = mongoose.model("Docente", DocenteSchema);
