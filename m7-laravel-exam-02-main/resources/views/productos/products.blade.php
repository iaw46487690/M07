@extends((Request::ajax()) ? 'layouts.ajax' : 'layouts.app')

@section('content')
<div class="row">
  <div class="col-lg-3">
    <!--TODO: SEARCH-->
    <h2 class="my-4">Buscar <br>(en este genero)</h2>
    <form method="POST" action="/search">
      {{ csrf_field() }}
      <input type="hidden" name="category" value="{{ Request()->id }}">
      <div class="form-group">
        <div class="form-check">
          <input name="search" class="form-text" type="text">
        </div>
      </div>
    </form>
    <!--Fin BLOQUE-->


    <!--TODO: SELECCIONAR GENERO-->
    <h2 class="my-4">Genero</h2>
        <div class="list-group">
            @foreach ($filters as $filter)
                @foreach ($filter as $key => $category)
                    <a href="{{ url('category', $key) }}" class="list-group-item">{{ $category }}</a>
                @endforeach
            @endforeach
        </div>
    <!--Fin BLOQUE-->

  </div>

  <div class="col-lg-9">
    <!--TODO: BANNER-->
    @if($showBanner)
    <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    @endif
    <!--Fin BLOQUE-->

    <!--TODO: LISTA DE PELICULAS-->
    <div class="row">
        @forelse ($products as $prod)
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100">
                    <a href="#"><img class="card-img-top" src="{{asset('img/') . '/' . $prod->image}}" alt=""></a>
                <div class="card-body">
                    <h4 class="card-title">
                    <a href="#">{{ $prod->name }}</a>
                    </h4>
                    <h5>${{ $prod->price }}</h5>
                    <p class="card-text">{{ $prod->descripcion }}</p>
                </div>
                <div class="card-footer">
                    <form class="addCart" action="/addToCart" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{ $prod->id }}">
                        <input type="hidden" name="name" value="{{ $prod->name }}">
                        <input type="hidden" name="category" value="{{ $prod->category }}">
                        <input type="hidden" name="descripcion" value="{{ $prod->descripcion }}">
                        <input type="hidden" name="rating" value="{{ $prod->rating }}">
                        <input type="hidden" name="stock" value="{{ $prod->stock }}">
                        <input type="hidden" name="price" value="{{ $prod->price }}">
                        <input type="hidden" name="image" value="{{ $prod->image }}">
                        <input type="submit" class="btn-block btn-primary" name="action" value="Comprar">
                        <input type="submit" class="btn-block btn-primary" name="action" value="Alquilar">
                    </form>
                </div>
                </div>
            </div>
        @empty
            <!--SI no hay productos mostrar:-->
            <p>No products to show</p>
        @endforelse
    </div>

    <!--Fin BLOQUE-->


  </div>

</div>

<!--TODO: ACCIONES DE CARRITO-->
<script>
  //Escribir aqui el codigo necesario de AXIOS
  //Al hacer click se obtiene la info del producto para saber si hay stock
  //Si hay stock se envia una petición para guardar en el carrito ese producto. SE guardara como comprado o alquilado
</script>
 <!--Fin BLOQUE-->

@endsection
