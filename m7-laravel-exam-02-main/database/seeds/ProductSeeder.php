<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'El señor de los anillos señor de los anillos',
            'category' => 1,
            'descripcion' => 'Algo de un anillo',
            'rating' => 4,
            'stock' => 7,
            'price' => 10.10,
            'image' => 'img01.jpg'
        ]);
        DB::table('products')->insert([
            'name' => 'Ironman',
            'category' => 2,
            'descripcion' => 'Un tio con mucha pasta',
            'rating' => 4,
            'stock' => 1,
            'price' => 5.00,
            'image' => 'img02.jpg'
        ]);
        DB::table('products')->insert([
            'name' => 'Passengers',
            'category' => 3,
            'descripcion' => 'Una nave espacial',
            'rating' => 2,
            'stock' => 3,
            'price' => 1.00,
            'image' => 'img03.jpg'
        ]);
        DB::table('products')->insert([
            'name' => 'V de Vendetta',
            'category' => 2,
            'descripcion' => 'Venganza',
            'rating' => 2,
            'stock' => 3,
            'price' => 1.00,
            'image' => 'img04.jpg'
        ]);
    }
}
