<!--Question: Crea una URL en la que pasemos el nombre y la edad del
usuario actual y posteriormente recuperalo con las superglobales $_GET-->
<?php
$nombre = $_GET['nombre'];
$edad = $_GET['edad'];
$ciudad = $_GET['ciudad'];
echo "Hola " .$nombre. " tienes " .$edad. " años y vives en ". $ciudad;
?>

<!--Question: Crea un formulario que envie a otra pagina los datos
personales de una persona en formato POST. Escribe el codigo del script
que recibe esta información para mostrarla en pantalla.-->

<form method="post" action="datos.php" >
Introduce tus datos: <br />
Nombre: <input type="text" name="nombre" /><br />
Apellido: <input type="text" name="apellido" /><br />
Telefono: <input type="text" name="telefono" /><br />
Direccion: <input type="text" name="direccion" /><br />
<input type="submit" value="Enviar formulario" />
</form>

<?php
$nombre = $_POST["nombre"];
$apellido = $_POST["apellido"];
$telefono = $_POST["telefono"]; 
$direccion = $_POST["direccion"];
echo "Bienvenido a la web, " . $nombre . " " . $apellido . ", <br />
Telefono: " . $telefono . ". <br/>Direccion: ".$direccion;
?>
