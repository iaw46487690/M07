<!--
  4. Crear un formulario en el que se introduce el precio de una
    compra y realizar el programa que calcula mediante una función
    la siguientes reglas:
    - Si la compra no alcanza los $100, no se realiza ningún descuento.
    - Si la compra está entre $100 y $499,99, se descuenta un 10%.
    - Si la compra supera los $500, se descuenta un 15%.
-->
<?php
$preu = $_POST["preu"];
function compra($preu) {
  switch ($preu) {
    case $preu < 100:
      break;
    case $preu >= 100 && $preu < 500:
    $descuento = 10;
      break;
    case $preu >= 500:
    $descuento = 15;
      break;
  }
  return $preu - ($preu / 100 * $descuento);
}
echo compra($preu) . " €";
?>
