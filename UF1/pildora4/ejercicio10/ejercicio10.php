<html>
  <head>
    <title>class CabeceraPagina</title>
  <!--
    10. Confeccionar una clase CabeceraPagina que permita mostrar un título,
    indicarle si queremos que aparezca centrado, a derecha o izquierda,
    además permitir definir el color de fondo y de la fuente. Todo esto
    son propiedades del objecto Titulo
  -->
  </head>
  <body>
    <?php
    class CabeceraPagina {
      private $alineacion;
      private $colorFondo;
      private $colorFuente;
      // Establece los valores de los atributos
      public function inicializar($ali, $cFo, $cFu) {
        $this->alineacion=$ali;
        $this->colorFondo=$cFo;
        $this->colorFuente=$cFu;
      }
      //Metodo que visualiza el menu en una pagina HTML
      public function mostrar() {
        echo '<div style="text-align:'. $this->alineacion .';
          background-color:'. $this->colorFondo .';color:'. $this->colorFuente .'">hols</div>';
      }
    }
    $c1=new CabeceraPagina();
    $c1->inicializar('left', 'yellow', 'red');
    $c1->mostrar();
    $c2=new CabeceraPagina();
    $c2->inicializar('right', 'blue', 'white');
    $c2->mostrar();
    $c3=new CabeceraPagina();
    $c3->inicializar('center', 'green', 'white');
    $c3->mostrar();
    ?>
  </body>
</html>
