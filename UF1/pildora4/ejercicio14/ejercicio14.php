<html>
  <head>
    <title>class CabeceraDePagina</title>
  <!--
    14. Codificar la clase CabeceraDePagina que nos muestre un título alineado
    con un determinado color de fuente y fondo. Definir en el constructor
    parámetros predeterminados para los colores de fuente, fondo y el
    alineado del título.
  -->
  </head>
  <body>
  <?php
  class CabeceraPagina {
    private $alineacion;
    private $colorFondo;
    private $colorFuente;
    private $texto;
    // Establece los valores de los atributos
    public function __construct() {
      $this->alineacion='left';
      $this->colorFondo='red';
      $this->colorFuente='blue';
      $this->texto='Hola';
    }
    //Metodo que visualiza el menu en una pagina HTML
    public function graficar() {
      echo '<div style="text-align:'. $this->alineacion .';
        background-color:'. $this->colorFondo .';
        color:'. $this->colorFuente .'">'. $this->texto .'</div>';
    }
  }
  $c1=new CabeceraPagina();
  $c1->graficar();
  ?>
  </body>
</html>
