<!--
   6. Realiza una función llamada intermedio(a, b) que a partir de dos
   números, devuelva su punto intermedio. Cuando lo tengas comprueba el
   punto intermedio entre -12 y 24
-->
<?php
$num1 = $_POST["num1"];
$num2 = $_POST["num2"];

function intermedio($num1, $num2) {
  return ($num1 + $num2) / 2;
}
echo intermedio($num1, $num2);
?>
