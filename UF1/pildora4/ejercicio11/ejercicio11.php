<html>
  <head>
    <title>class CabeceraPagina</title>
  <!--
    11. Confeccionar una clase CabeceraPagina que permita mostrar un título,
    indicarle si queremos que aparezca centrado, a derecha o izquierda,
    además permitir definir el color de fondo y de la fuente. Pasar los
    valores que cargaran los atributos mediante un constructor.
  -->
  </head>
  <body>
    <?php
    class CabeceraPagina {
      private $alineacion;
      private $colorFondo;
      private $colorFuente;
      private $texto;
      // Establece los valores de los atributos
      public function __construct($ali, $cFo, $cFu, $tex) {
        $this->alineacion=$ali;
        $this->colorFondo=$cFo;
        $this->colorFuente=$cFu;
        $this->texto=$tex;
      }
      //Metodo que visualiza el menu en una pagina HTML
      public function mostrar() {
        echo '<div style="text-align:'. $this->alineacion .';
          background-color:'. $this->colorFondo .';
          color:'. $this->colorFuente .'">'. $this->texto .'</div>';
      }
    }
    $c1=new CabeceraPagina('left', 'yellow', 'red', 'Alejandro');
    $c1->mostrar();
    $c2=new CabeceraPagina('right', 'blue', 'white', '20');
    $c2->mostrar();
    $c3=new CabeceraPagina('center', 'green', 'white', 'M07');
    $c3->mostrar();
    ?>
  </body>
</html>
