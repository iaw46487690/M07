<!--
  8. Confeccionar una clase Empleado, definir como atributos su nombre y
  sueldo. Definir un método inicializar que lleguen como dato el nombre y
  sueldo. Plantear un segundo método que imprima el nombre y un mensaje si
  debe o no pagar impuestos (si el sueldo supera a 3000 paga impuestos)
-->
<?php
class Empleado {
  // Atributos
  private $nombre;
  private $sueldo;
  // Metodo inicializar()
  public function inicializar($name, $salary)
  {
    $this->nombre=$name;
    $this->sueldo=$salary;
  }
// Metodo imprimir()
  public function imprimir()
  {
    echo $this->nombre;
    echo '<br>';
    if ($this->sueldo > 3000) {
      echo 'Debe pagar impuestos';
    } else {
      echo 'No debe pagar impuestos';
    }
    echo '<br>';
    echo '<br>';
  }
}
// Empleado 1
$emp1=new Empleado();
$emp1->inicializar('Alejandro', 2000);
$emp1->imprimir();
// Empleado 2
$emp2=new Empleado();
$emp2->inicializar('Thomas', 4000);
$emp2->imprimir();
?>
