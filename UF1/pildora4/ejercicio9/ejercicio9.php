<html>
  <head>
    <title>class Hiperviculo</title>
  <!--
    9. Implementar una clase que muestre una lista de hipervínculos en forma
    horizontal (básicamente un menú de opciones)

    En este caso no hace falta formulario

    //Metodo que añade un nuevo enlace al menu
    cargarOpcion()
    //Metodo que visualiza el menu en una pagina HTML
    mostrar()
  -->
  </head>
  <body>
    <ul>
    <?php
    class Hipervinculo {
      private $enlace;
      private $nombre;
      //Metodo que añade un nuevo enlace al menu
      public function cargarOpcion($enl, $nom) {
        $this->enlace=$enl;
        $this->nombre=$nom;
      }
      //Metodo que visualiza el menu en una pagina HTML
      public function mostrar() {
        echo '<li><a href="'. $this->enlace .'"target="_blank">'. $this->nombre .'</a></li>';
      }
    }
    $h1=new Hipervinculo();
    $h1->cargarOpcion('https://www.youtube.com/?hl=es&gl=ES', 'Youtube');
    $h1->mostrar();
    $h2=new Hipervinculo();
    $h2->cargarOpcion('https://www.google.com/', 'Google');
    $h2->mostrar();
    ?>
  </ul>
  </body>
</html>
