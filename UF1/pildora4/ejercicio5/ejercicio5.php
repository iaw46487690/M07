<!--
   5. Realiza una función llamada relacion(a, b) que a partir de dos
  números cumpla lo siguiente:
   - Si el primer número es mayor que el segundo, debe devolver 1.
   - Si el primer número es menor que el segundo, debe devolver -1.
   - Si ambos números son iguales, debe devolver un 0.
-->
<?php
$num1 = $_POST["num1"];
$num2 = $_POST["num2"];

function relacion($num1, $num2) {
  if ($num1 > $num2) {
    $solucio = 1;
  } else if ($num1 < $num2) {
    $solucio = -1;
  } else {
    $solucio = 0;
  }
  return $solucio;
}
echo relacion($num1, $num2);
?>
