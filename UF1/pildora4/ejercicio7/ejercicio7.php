<!--
  7. Realiza una función separar(lista) que tome una lista de
  números enteros y devuelva (no imprimir) dos listas ordenadas.
  La primera con los números pares y la segunda con los números impares.
  numeros = [-12, 84, 13, 20, -33, 101, 9]

  Resultado:
  [-12, 20, 84]
  [-33, 9, 13, 101]
-->
<?php
$numeros = $_POST["numeros"];
function separar($numeros) {
  $j = 0;
  $k = 0;
  // Buble para separar pares de impares en 2 arrays
  for ($i = 0; $i < count($numeros); $i++) {
    if ($numeros[$i] % 2 == 0) {
      $pares[$j] += $numeros[$i];
      $j++;
    } else {
      $impares[$k] += $numeros[$i];
      $k++;
    }
  }
  // Ordenar los 2 arrays
  sort($pares);
  sort($impares);
  // Mostrar array par
  $par = "[" . $pares[0];
  for ($i = 1; $i < count($pares); $i++) {
    $par = $par . ", " . $pares[$i];
  }
  $par = $par . "]";
  // Mostrar array impar
  $impar = "[" . $impares[0];
  for ($i = 1; $i < count($impares); $i++) {
    $impar = $impar . ", " . $impares[$i];
  }
  $impar = $impar . "]";
  // return
  return "Resultado:<br />" . $par . "<br />" . $impar;
}
echo separar($numeros);
?>
