<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductSeeder::class);
    }
}

class ProductSeeder extends Seeder
{
    public function run(){
        DB::table('products')->insert([
            'name'=>'lego1',
            'img'=>'lego1.jpeg',
            'price'=>'12.50€',
            'description'=>'A lego game',
            'score'=>'4',
            'category'=>'lego',
        ]);

        DB::table('products')->insert([
            'name'=>'lego2',
            'img'=>'lego2.jpeg',
            'price'=>'20.00€',
            'description'=>'A card game',
            'score'=>'3',
            'category'=>'card',
        ]);

        DB::table('products')->insert([
            'name'=>'lego3',
            'img'=>'lego3.jpeg',
            'price'=>'22.50€',
            'description'=>'An answer game',
            'score'=>'1',
            'category'=>'answer',
        ]);

        DB::table('products')->insert([
            'name'=>'lego4',
            'img'=>'lego4.jpeg',
            'price'=>'32.50€',
            'description'=>'A funny game',
            'score'=>'2',
            'category'=>'role',
        ]);
    }
}
