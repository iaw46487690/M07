<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopeStars($query, $product) {
        return $query->where('score', $product);
    }

    public function scopeCategory($query, $product) {
        return $query->where('category', $product);
    }
}
