<!-- Question: Crea un script para mostrar la información del servidor que
lo ejecuta -->
<?php phpinfo(); ?>

<!-- Question: Crea un script para que muestre un texto de cabecera h1 y un
parrafo por pantalla -->
<?php echo '<h1>Hello World</h1><p>Mi primer programa</p>'; ?>

<!-- Question: Crea un saludo que cambie en función del nombre almacenado en
una variable $name -->
<?php
$autor = "Alejandro";
echo "<h1>¡Hola!</h1>
<p>Esta página ha sido creada por $autor.</p>";
?>

<!--Haz un programa que realize en una misma sentencia un conjunto de
diferentes operaciones aritmeticas utilizando tanto numero como valores
almacenados en variables.-->
<?php
$numero1 = 1.7;
$numero2 = 6.2;
$resultado = ($numero1 + 5) / 3 * ($numero2 - 10) * pow(3,2);
echo "Resultado: " . $resultado;
?>
