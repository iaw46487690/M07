<html>
  <head>
    <title>PHP Test</title>
      <!--2. Crea una pagina en la que se almacenan dos numeros en variables
      y muestre en una tabla los valores que toman las variables y cada
      uno de los resultados de todas las operaciones aritmeticas.-->
  </head>
  <body>
    <?php
      $A = 4;
      $B = 3;
      $suma = $A + $B;
      $resta = $A - $B;
      $multiplicacio = $A * $B;
      $divisio = $A / $B;
      $exponent = pow($A,$B);
    ?>
        <table style="width:100%">
          <tr>
            <th>Operacion</th>
            <th>Valor</th>
          </tr>
          <tr>
            <td>A</td><td><?php echo "$A" ?></td>
          </tr>
          <tr>
            <td>B</td><td><?php echo "$B" ?></td>
          </tr>
          <tr>
            <td>A+B</td><td><?php echo "$suma" ?></td>
          </tr>
          <tr>
            <td>A-B</td><td><?php echo "$resta" ?></td>
          </tr>
          <tr>
            <td>A*B</td><td><?php echo "$multiplicacio" ?></td>
          </tr>
          <tr>
            <td>A/B</td><td><?php echo "$divisio" ?></td>
          </tr>
          <tr>
            <td>AexpB</td><td><?php echo "$exponent" ?></td>
          </tr>
        </table>
      <!--echo '<p>A+B  '.($A+$B).'</p>';-->
  </body>
</html>
