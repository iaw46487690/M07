<!DOCTYPE html>
<html>
    <head>
        <style>
            table, th, tr, td {
                border: 2px solid black;
                text-align: center;
                border-collapse: collapse;
            }
        </style>
    </head>
    <body>
        <?php
        $goles = $_POST['goles'];
        $jugadores = $_POST['jugadores'];
        $partidos = $_POST['partidos'];
        $numJugadores = $_POST['numJugadores'];
        $tabla = '<table style="width:100%"><tr><th>Jugador</th>';
        for ($i = 1; $i <= $partidos; $i++) {
          $tabla = $tabla . '<th>Partido'.$i.'</th>';
        }
        $tabla = $tabla . '</tr>';
        $k = 0;
        for ($y = 0; $y < $numJugadores; $y++) {
          $tabla = $tabla . '<tr><td>'.$jugadores[$y].'</td>';
          for ($j = 0; $j < $partidos; $j++) {
            $tabla = $tabla . '<td>'.$goles[$k].'</td>';
            $k ++;
          }
          $tabla = $tabla . '</tr>';
        }
        $tabla = $tabla . '</table>';
        echo $tabla;
        ?>
    </body>
</html>
